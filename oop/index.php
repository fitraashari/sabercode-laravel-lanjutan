<?php
require('hewan.php');
require('fight.php');

class Elang{
    use hewan;
    use fight;
    public $jenis = "Elang";
    public function __construct($nama="Elang")
    {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->deffencePower = 5;
    }
    public function getInfoHewan(){
        $detail = "Nama Hewan: ".$this->nama."<br>";
        $detail .= "Darah: ".$this->darah."<br>";
        $detail .= "Jumlah Kaki: ".$this->jumlahKaki."<br>";
        $detail .= "Keahlian: ".$this->keahlian."<br>";
        $detail .= "Attack Power: ".$this->attackPower."<br>";
        $detail .= "Deffence Power: ".$this->deffencePower."<br>";
        $detail .= "Jenis: ".$this->jenis."<br>";
        return $detail;
    }
}
class Harimau{
    use hewan;
    use fight;
    public $jenis = "Harimau";
    public function __construct($nama="Harimau")
    {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->deffencePower = 8;
    }
    public function getInfoHewan(){
        $detail = "Nama Hewan: ".$this->nama."<br>";
        $detail .= "Darah: ".$this->darah."<br>";
        $detail .= "Jumlah Kaki: ".$this->jumlahKaki."<br>";
        $detail .= "Keahlian: ".$this->keahlian."<br>";
        $detail .= "Attack Power: ".$this->attackPower."<br>";
        $detail .= "Deffence Power: ".$this->deffencePower."<br>";
        $detail .= "Jenis: ".$this->jenis."<br>";
        return $detail;
    }
}

//statistik awal
echo "<h3>Statistik Awal</h3>";

echo "<p style='border:1px solid black'>";
$elang1 = new Elang("Elang1");
echo $elang1->getInfoHewan();
echo "<br>";
echo $elang1->atraksi();
echo "</p>";

echo "<p style='border:1px solid black'>";
$harimau1 = new Harimau("Harimau1");
echo $harimau1->getInfoHewan();
echo "<br>";
echo $harimau1->atraksi();
echo "</p>";

//elang menyerang harimau
echo "<hr style='height:3px;background-color:red'><p style='color:red'>";
echo $elang1->serang($harimau1);
echo "</p><hr style='height:3px;background-color:red'>";
echo "<hr>";

//statistik setelah penyerangan
echo "<h3>Statistik Setelah Bakuhantam</h3>";
echo "<p style='border:1px solid black'>";
echo $elang1->getInfoHewan();
echo "</p>";
echo "<p style='border:1px solid black'>";
echo $harimau1->getInfoHewan();
echo "</p>";
